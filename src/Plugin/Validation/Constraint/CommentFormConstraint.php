<?php

namespace Drupal\comment_limit\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Supports validating comment form.
 *
 * @Constraint(
 * id = "CommentFormConstraint",
 * label = @Translation("CommentFormConstraint", context = "Validation")
 * )
 */
class CommentFormConstraint extends Constraint {

  /**
   * Entity id.
   *
   * @var string
   */
  public $entityId;

  /**
   * Entity type.
   *
   * @var string
   */
  public $entityType;

  /**
   * Field id.
   *
   * @var string
   */
  public $fieldId;

  /**
   * Field name.
   *
   * @var string
   */
  public $fieldName;

}
