# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

Limit comments per field and user

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/comment_limit>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/comment_limit>


## REQUIREMENTS

Drupal core "comment" module


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Structure > Content types. 
    3. Select your content type with comments field.
    4. Set limits.
    5. Save configuration.


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
